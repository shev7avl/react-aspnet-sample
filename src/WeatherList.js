import React from 'react';
import Forecast from './Forecast';
import axios from 'axios';

export default class WeatherList extends React.Component {
  state = {
    forecasts: []
  }

  componentDidMount() {
    axios.get(`https://localhost:7008/WeatherForecast`)
      .then(res => {
        const forecasts = res.data;
        this.setState({ forecasts });
      })
  }

  render() {
    return (
      <ul>
        { this.state.forecasts.map(forecast => Forecast(forecast)) }
      </ul>
    )
  }
}
