import React from 'react';
import Button from 'react-bootstrap/Button';

function Forecast(props) {
  return (
    <div className="d-grid gap-2">
      <Button variant="primary" size="lg">
        Date: {props.date}
      </Button>
      <Button variant="secondary" size="sm">
        Temperature, C: {props.temperatureC}
      </Button>
      <Button variant="secondary" size="sm">
        Temperature, F: {props.temperatureF}
      </Button>
      <Button variant="secondary" size="sm">
        Summary: {props.summary}
      </Button>
    </div>
  )
}

export default Forecast