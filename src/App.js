import logo from './logo.svg';
import './App.css';
import WeatherList from './WeatherList';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <main className="App">
      <WeatherList />
    </main>
    
  );
}

export default App;
